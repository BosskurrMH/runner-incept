# This is a mirror of the actual runner-incept work happening.

## What is going on here?

Welcome to a glorious abuse of the `needs` keyword, docker-in-docker, and probably a few other features of GitLab CI.

Everything is happening in [pipelines](https://gitlab.com/erushton/runner-incept/-/pipelines). A quick look shows several stages. The first is where things get weird - the `Runners` stage has separate jobs for different runner configs. For each job, the script downloads the lastest build of `main`, and then executes and registers the runner to this project. Each registered runner is isolated with specific tags. Downstream jobs are then tagged to execute their jobs on those upstream registered runners - thus exercising the runner functionality as it appears on the `main` branch. "Runners" (aka jobs in the `Runners` stage) are killed after a timeout so that their job in the pipeline appears as a success. If they failed to launch then the downstream jobs would never pass so this shouldn't be possible to introduce any false-positives. After the "Runners" timeout expires the individual runners are then unregistered from the project.

Currently the `.gitlab-ci.yml` is written to make the concepts that are happening clearer instead of trying to de-duplicate things. There's obviously a ton of opportunity to remove duplication of job definitions etc.

## Layout

There are three types of "job" being run. And each type is declared in a high-level `.gitlab-ci.yml` file included from the main `.gitlab-ci.yml` file.

1. Runners - found in `runners.gitlab-ci.yml` - these contain definitions of Runners-under-test and their configuration. So maybe it's a Runner on Windows 1809 with the Shell executor. Or maybe it's a Runner on Linux, using the Docker executor, with Powershell as the shell language. These Runners-under-test are defined in this file. Each "job" is an individual Runner and pulls the appropriate binary built off of `main`, does a test execution with the `--version` flag showing what it's build environment and version are, and then registers and runs a runner with the given config against this project. In an `after_script` section the Runner is unregistered. The length of time the Runner is run for is controlled by a timeout mechanism that's OS dependent.

1. Scenarios - these are jobs that are run on the Runners-under-test. A scenario is literally just a CI job defined in the usual yaml that we want to execute against one of the Runners-under-test.

1. Validations - theese are jobs that check on some sort of result from a specific scenario. So for example, we might want to check that `shell-hello-world` scenario does indeed mask variables correctly. The only way to do that is to examine the scenario's job log, so that's what the validation does. In some cases scenarios provide their own defacto validations. We might not need to inspect anything in a Linux s390x scenario knowing that if the scenario job successfully passes it's all the validation we need.

## Timeouts

Timeouts are important in these jobs to prevent them malfunctioning and should be kept reasonably tight. Windows needs much more generous timeouts in general so those should be overriden at the job level. To refresh on the timeout hierarchy refer to the [timeout docs](https://docs.gitlab.com/ee/ci/yaml/#timeout).
