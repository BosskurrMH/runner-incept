require 'rspec'
require_relative '../tanuki/tanuki.rb'

tanuki = Tanuki.new

describe "Arm64 runner" do
    it "runs as an arm64 binary" do
        trace = tanuki.get_job_trace('arm64-linux-shell-runner-incept')
        expect(trace).to include('OS/Arch:      linux/arm64')
    end   
end

describe "Arm runner" do
    it "runs as an arm binary" do
        trace = tanuki.get_job_trace('arm-linux-shell-runner-incept')
        expect(trace).to include('OS/Arch:      linux/arm')
    end   
end

# describe "s390x runner" do
#     it "runs as an s390x binary" do
#         trace = tanuki.get_job_trace('s390x-linux-shell-runner-incept')
#         expect(trace).to include('OS/Arch:      linux/s390x')
#     end   
# end
