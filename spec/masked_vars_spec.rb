require 'rspec'
require_relative '../tanuki/tanuki.rb'

tanuki = Tanuki.new

describe "Shell executor Secret Masking" do
    it "masks the variable and doesn't display it's value" do
        trace = tanuki.get_job_trace('shell-hello-world')
        expect(trace).to include('[MASKED]')
        expect(trace).not_to include('BRUCE_WAYNE')

        trace = tanuki.get_job_trace('shell-hello-world-amd64-fips')
        expect(trace).to include('[MASKED]')
        expect(trace).not_to include('BRUCE_WAYNE')
    end
    
end

describe "Docker executor Secret Masking" do
    it "masks the variable and doesn't display it's value in powershell" do
        trace = tanuki.get_job_trace('pwsh-docker-hello-world')
        expect(trace).to include('[MASKED]')
        expect(trace).not_to include('BRUCE_WAYNE')
    end
    it "masks the variable and doesn't display it's value in the default shell" do
        trace = tanuki.get_job_trace('docker-hello-world')
        expect(trace).to include('[MASKED]')
        expect(trace).not_to include('BRUCE_WAYNE')
    end
end

describe "Windows docker-executor 1809 Secret Masking" do
    it "masks the variable and doesn't display it's value" do
        trace = tanuki.get_job_trace('windows-1809-docker-hello-world')
        # Windows jobs will only run for the main branch of the parent pipeline
        if !trace.to_s.strip.empty?
            expect(trace).to include('[MASKED]')
            expect(trace).not_to include('BRUCE_WAYNE')
        end
    end
end

describe "Windows shell-executor cmd Secret Masking" do
    it "masks the variable and doesn't display it's value" do
        trace = tanuki.get_job_trace('windows-1809-cmd-hello-world')
        # Windows jobs will only run for the main branch of the parent pipeline
        if !trace.to_s.strip.empty?
            expect(trace).to include('[MASKED]')
            expect(trace).not_to include('BRUCE_WAYNE')    
        end
    end
end

