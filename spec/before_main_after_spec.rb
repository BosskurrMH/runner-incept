require 'rspec'
require_relative '../tanuki/tanuki.rb'

tanuki = Tanuki.new

describe "FIPS message printed by Runner " do
    it "prints the message" do
        trace = tanuki.get_job_trace('fips-amd64-runner-incept')
        expect(trace).to include('FIPS mode enabled.')
    end
end

describe "Shell executor script steps" do
    it "executes before, main, and after scripts" do
        trace = tanuki.get_job_trace('shell-hello-world')
        expect(trace).to include('this is the before script')
        expect(trace).to include('this is the main script')
        expect(trace).to include('this is the after script')

        trace = tanuki.get_job_trace('shell-hello-world-amd64-fips')
        expect(trace).to include('this is the before script')
        expect(trace).to include('this is the main script')
        expect(trace).to include('this is the after script')
    end
end

describe "Docker executor script steps" do
    it "executes before, main, and after scripts with default shell" do
        trace = tanuki.get_job_trace('docker-hello-world')
        expect(trace).to include('this is the before script')
        expect(trace).to include('this is the main script')
        expect(trace).to include('this is the after script')
    end
    it "executes before, main, and after scripts with power shell" do
        trace = tanuki.get_job_trace('pwsh-docker-hello-world')
        expect(trace).to include('this is the before script')
        expect(trace).to include('this is the main script')
        expect(trace).to include('this is the after script')
    end
end

describe "Custom executor script steps" do
    it "executes before, main, and after scripts" do
        trace = tanuki.get_job_trace('custom-hello-world')
        expect(trace).to include('this is the before script')
        expect(trace).to include('this is the main script')
        expect(trace).to include('this is the after script')
    end
end

describe "Windows docker executor script steps" do
    it "executes before, main, and after scripts" do
        trace = tanuki.get_job_trace('windows-1809-docker-hello-world')
        # Windows jobs will only run for the main branch of the parent pipeline
        if !trace.to_s.strip.empty?
            expect(trace).to include('this is the before script')
            expect(trace).to include('this is the main script')
            expect(trace).to include('this is the after script')
        end        
    end
end

describe "Windows shell executor script steps" do
    it "executes before, main, and after scripts" do
        trace = tanuki.get_job_trace('windows-1809-cmd-hello-world')
        # Windows jobs will only run for the main branch of the parent pipeline
        if !trace.to_s.strip.empty?
            expect(trace).to include('this is the before script')
            expect(trace).to include('this is the main script')
            expect(trace).to include('this is the after script')    
        end
    end
end
